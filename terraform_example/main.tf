resource "aci_tenant" "prod-dmz" {
  name        = "PROD-DMZ"
  description = "Tenant PROD-DMZ created by Terraform"
}
resource "aci_application_profile" "prod-dmz_prod_dmz_app" {
  tenant_dn   = aci_tenant.prod-dmz.id
  name        = "PROD_DMZ_APP"
  description = "PROD_DMZ_APP Migration using Terraform"
}
resource "aci_vrf" "prod-dmz_prod_dmz_app_prod_dmz_vrf" {
  tenant_dn = aci_tenant.prod-dmz.id
  name      = "PROD_DMZ_VRF"
}
resource "aci_bridge_domain" "prod-dmz_prod_dmz_app_prod_dmz_bd" {
  tenant_dn          = aci_tenant.prod-dmz.id
  relation_fv_rs_ctx = aci_vrf.prod-dmz_prod_dmz_app_prod_dmz_vrf.id
  name               = "PROD_DMZ_BD"
  arp_flood                      = "yes"
  description                    = "BD for VLAN PROD_DMZ"
  unk_mac_ucast_act              = "flood"
  unk_mcast_act                  = "flood"
  multi_dst_pkt_act              = "bd-flood"
  unicast_route                  = "yes"
  host_based_routing             = "yes"
}
resource "aci_subnet" "prod_dmz_bd_subnet" {
  depends_on = [aci_bridge_domain.prod-dmz_prod_dmz_app_prod_dmz_bd]
  #bridge_domain_dn = aci_bridge_domain.prod-dmz_prod_dmz_app_prod_dmz_bd.id
  parent_dn = aci_bridge_domain.prod-dmz_prod_dmz_app_prod_dmz_bd.id
  name_alias             = "VLAN_1113_subnet"
  scope              = ["public"]
  ip               = "10.11.211.1/24"
  description      = "VLAN_1113_subnet"
}
resource "aci_rest" "api1_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf" {
  depends_on = [aci_subnet.prod_dmz_bd_subnet]
  path       = "/api/alias/mo/uni/tn-PROD-DMZ/ctx-PROD_DMZ_VRF.json?clear=yes"
  payload = <<EOF
  {}
  EOF
}
resource "aci_rest" "api2_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf" {
  depends_on = [aci_rest.api1_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf]
  path       = "/api/node/mo/uni/tn-PROD-DMZ/ctx-PROD_DMZ_VRF/any.json"
  payload = <<EOF
  {
    "vzAny": {
      "attributes": {
        "dn": "uni/tn-PROD-DMZ/ctx-PROD_DMZ_VRF/any",
        "prefGrMemb": "enabled"
      },
      "children": []
    }
  }
  EOF
}
resource "aci_rest" "bd_clear_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf" {
  depends_on = [aci_rest.api2_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf]
  path       = "/api/alias/mo/uni/tn-PROD-DMZ/BD-PROD_DMZ_BD.json?clear=yes"
  payload = <<EOF
  {}
  EOF
}
resource "aci_rest" "bd_attach_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf" {
  depends_on = [aci_rest.bd_clear_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf]
  path       = "/api/node/mo/uni/tn-PROD-DMZ/BD-PROD_DMZ_BD/rsctx.json"
  payload = <<EOF
  {
    "fvRsCtx": {
      "attributes": {
        "tnFvCtxName": "PROD_DMZ_VRF"
      },
      "children": []
    }
  }
  EOF
}
resource "aci_application_epg" "epg_prod_dmz" {
  depends_on = [aci_rest.bd_attach_prod-dmz_prod_dmz_app_prod_dmz_bd_prod_dmz_vrf]
  application_profile_dn  = aci_application_profile.prod-dmz_prod_dmz_app.id
  name                    = "VLAN_1113_EPG"
  pc_enf_pref                 = "unenforced"
  pref_gr_memb                = "include"
  relation_fv_rs_bd      =  aci_bridge_domain.prod-dmz_prod_dmz_app_prod_dmz_bd.id
}
